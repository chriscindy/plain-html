(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
  FB.init({
    appId            : '1460963937280795',
    autoLogAppEvents : true,
    xfbml            : true,
    version          : 'v2.9'
  });
  FB.AppEvents.logPageView();
  var feed = document.getElementById('facebook-feed'),
      dialog = document.getElementById('facebook-dialog'),
      send = document.getElementById('facebook-send');              
  feed.onclick = function(e) {
    FB.ui({
      method: 'feed',
      link: 'https://developers.facebook.com/docs/',
      caption: '滴滴一下，马上出发',
      picture: 'http://static.diditaxi.com.cn/api/img/logo_new_99_share.png',
      description: '滴滴打车是时下最热的手机打车神器。覆盖最广，用户最多。实时叫车，百秒应答，享受专车待遇，马上下载试试吧~',
      display: 'popup'
    });
  }
  dialog.onclick = function(e) {
    FB.ui({
        method: 'share',
        mobile_iframe: true,
        href: 'https://developers.facebook.com/docs/'
      });
    }
  send.onclick = function(e) {
    FB.ui({
      method: 'send',
      link: 'https://developers.facebook.com/docs/'
    });              
  }
};

var facebookDialogJs = $('#facebook-dialog-js'),
    facebookFeedJs = $('#facebook-feed-js'),
    facebookSendJs = $('#facebook-send-js');

facebookDialogJs.click(function(){
  alert('facebook dialog');
  window.location.href = 'https://www.facebook.com/dialog/share?app_id=1460963937280795&amp;display=popup&amp;href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2F&amp;redirect_uri=https%3A%2F%2Fdevelopers.facebook.com%2Ftools%2Fexplorer'
})

facebookFeedJs.click(function(){
  alert('facebook feed');
  window.location.href = 'https://www.facebook.com/dialog/feed?app_id=1460963937280795&amp;display=popup&amp;caption=An%20example%20caption&amp;link=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2F&amp;redirect_uri=https://developers.facebook.com/tools/explorer'
})

facebookSendJs.click(function(){
  alert('facebook send');
  window.location.href = 'http://www.facebook.com/dialog/send?app_id=1460963937280795&amp;link=http://www.nytimes.com/interactive/2015/04/15/travel/europe-favorite-streets.html&amp;redirect_uri=https://www.domain.com/'
})
