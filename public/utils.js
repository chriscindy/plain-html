var ua = window.navigator.userAgent;
var isIOS = /(iphone|ipod|ios|ipad)/i.test(ua);

function urlParse (url) {
    url = url || location.search;
    var obj = {};
    var arr = url.match(new RegExp("[\?\&][^\?\&]+=[^\?\&]+", "g"));
    each(arr, function (item) {
        var _obj = item.substring(1).split('=');
        var key = decodeURIComponent(_obj[0]);
        var val = decodeURIComponent(_obj[1]);
        obj[key] = val;
    });
    return obj;
};

function typeEqual(obj, type) {
    return Object.prototype.toString.call(obj) === '[object ' + type + ']';
}

function isFn (obj) {
    return typeEqual(obj, 'Function');
};

function isObj (obj) {
    return typeEqual(obj, 'Object');
};

function isArray (obj) {
    return typeEqual(obj, 'Array');
};

function each (obj, fn, context) {
    if (!obj || !isFn(fn))
        return;
    context = context || obj;
    if (obj.length) {
        for (var i = 0, l = obj.length; i < l; i++) {
            if (false === fn.call(context, obj[i], i, obj))
                return;
        }
    } else if (isObj(obj)) {
        for (var n in obj) {
            if (false === fn.call(context, obj[n], n, obj))
                return;
        }
    }
}
